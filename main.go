package main

import (
	"bufio"
	"fmt"
	"github.com/hypebeast/go-osc/osc"
	"github.com/jessevdk/go-flags"
	"github.com/warthog618/gpio"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"
)

var options struct {
	Address  string `short:"o" long:"osc-dest" description:"Address to send OSC feedback to" default:"255.255.255.255:1245"`
	Config   string `short:"c" long:"config" description:"Config file for mapping gpio pins to OSC messages" default:"osc.conf"`
	Debounce uint   `short:"t" long:"debounce" description:"Time in milliseconds to ignore button bounces" default:"20"`
}

type pinState struct {
	lastTrigger time.Time
	lastState   gpio.Level
}

var gpio_to_pin = map[int]uint8{
	2:  gpio.GPIO2,
	3:  gpio.GPIO3,
	4:  gpio.GPIO4,
	5:  gpio.GPIO5,
	6:  gpio.GPIO6,
	7:  gpio.GPIO7,
	8:  gpio.GPIO8,
	9:  gpio.GPIO9,
	10: gpio.GPIO10,
	11: gpio.GPIO11,
	12: gpio.GPIO12,
	13: gpio.GPIO13,
	14: gpio.GPIO14,
	15: gpio.GPIO15,
	16: gpio.GPIO16,
	17: gpio.GPIO17,
	18: gpio.GPIO18,
	19: gpio.GPIO19,
	20: gpio.GPIO20,
	21: gpio.GPIO21,
	22: gpio.GPIO22,
	23: gpio.GPIO23,
	24: gpio.GPIO24,
	25: gpio.GPIO25,
	26: gpio.GPIO26,
	27: gpio.GPIO27,
}

var parser = flags.NewParser(&options, flags.Default)
var gpio_config map[uint8]string
var pinStates map[uint8]pinState

func main() {
	if _, err := parser.Parse(); err != nil {
		if flagsErr, ok := err.(*flags.Error); ok && flagsErr.Type == flags.ErrHelp {
			os.Exit(0)
		} else {
			os.Exit(1)
		}
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Kill)
	defer signal.Stop(quit)

	gpio_config = make(map[uint8]string)
	pinStates = make(map[uint8]pinState)

	configFile, err := os.Open(options.Config)
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(configFile)
	for scanner.Scan() {
		fields := strings.Fields(scanner.Text())
		if len(fields) != 2 {
			fmt.Printf("Error parsing config file around: %v\n", scanner.Text())
			os.Exit(1)
		} else {
			if pin, err := strconv.Atoi(fields[0]); err == nil {
				gpio_config[gpio_to_pin[pin]] = fields[1]
			} else {
				fmt.Printf("Pin number error: %v\n", fields[0])
				os.Exit(1)
			}
		}
	}

	if err := gpio.Open(); err != nil {
		panic(err)
	}
	defer gpio.Close()

	for pin, msg := range gpio_config {
		log.Printf("Adding interrupt for GPIO %v sending OSC message %v\n", pin, msg)
		p := gpio.NewPin(pin)
		p.Input()
		p.PullUp()
		pinStates[pin] = pinState{
			lastTrigger: time.Now(),
			lastState:   true,
		}
		if err := p.Watch(gpio.EdgeFalling, pinHandler); err != nil {
			panic(err)
		}
		defer p.Unwatch()
	}

	select {
	case <-quit:
	}
}

func sendOSC(msg string) {
	if !strings.Contains(options.Address, "255.255.255.255") {
		singleAddr(msg)
	} else {
		broadcastAll(msg)
	}
}

func singleAddr(msg string) {
	log.Printf(" -> Trying single address: %v\n", options.Address)
	udpConns := make([]*net.UDPConn, 0)

	if udpAddr, err := net.ResolveUDPAddr("udp", options.Address); err != nil {
		log.Printf(" -> Failed to resolve osc address: %v", err)
	} else if udpConn, err := net.DialUDP("udp", nil, udpAddr); err != nil {
		log.Printf("   -> Failed to open osc address: %v", err)
	} else {
		log.Printf("OSC: sending to %v", options.Address)
		udpConns = append(udpConns, udpConn)
	}
	sendMsg(msg, udpConns)
}

func broadcastAll(msg string) {
	log.Printf(" -> Broadcasting to all interfaces\n")
	port := strings.Join(strings.Split(options.Address, ":")[1:], "")

	udpConns := make([]*net.UDPConn, 0)

	addrs, _ := net.InterfaceAddrs()
	for _, addr := range addrs {
		ip, n, err := net.ParseCIDR(addr.String())
		if err != nil {
			log.Printf(" -> error parsing network: %v\n", err)
		} else {
			if ip.IsLoopback() {
				// Ignore loopback interfaces
				continue
			} else if ip.To4() != nil {
				broadcast := net.IP(make([]byte, 4))
				for i := range n.IP {
					broadcast[i] = n.IP[i] | (^n.Mask[i])
				}
				log.Printf(" -> using broadcast address %v", broadcast)

				dest := fmt.Sprintf("%v:%v", broadcast, port)

				if udpAddr, err := net.ResolveUDPAddr("udp", dest); err != nil {
					log.Printf(" -> Failed to resolve broadcast address %v: %v", dest, err)
				} else if udpConn, err := net.DialUDP("udp", nil, udpAddr); err != nil {
					log.Printf("   -> Failed to open broadcast address %v: %v", dest, err)
				} else {
					log.Printf("OSC: sending to %v", dest)
					udpConns = append(udpConns, udpConn)
				}
			}
		}
	}
	sendMsg(msg, udpConns)
}

func sendMsg(msg string, udpConns []*net.UDPConn) {
	packet := osc.NewMessage(msg)
	data, _ := packet.MarshalBinary()
	log.Printf("Writing data to connections\n")
	for _, conn := range udpConns {
		if _, err := conn.Write(data); err != nil {
			log.Printf(" -> Error writing to udp connection %v", conn)
		}
	}
}

// Handle pin changes
func pinHandler(pin *gpio.Pin) {
	value := pin.Read()
	p := pin.GetPin()
	log.Printf("Pin %v changed to %v\n", p, value)
	last := pinStates[p]
	bounceTime := last.lastTrigger.Add(time.Duration(options.Debounce) * time.Millisecond)
	if value == false && last.lastState == true && time.Now().After(bounceTime) {
		last.lastTrigger = time.Now()
		sendOSC(gpio_config[p])
	}
	last.lastState = value
	pinStates[p] = last
}
